# -*- coding:utf-8 -*-

from config import strings
from turnToAnotherService import matchLengthMessage, readMessageFromSocket, sendMessage
import json
from DataBaseConnector.Database import Stores

def update(city="Курган"):
    try:
        organizations_list = sendMessage(host=strings.HOST_ORGANIZATIONS,
                                         port=int(strings.PORT_ORGANIZATIONS),
                                         message='{"Table": "Point", "Query": "Select", "TypeParameter": "AllCity", "Values":["%s"]}' % city,
                                         expected_response="sequence")
        for organization in organizations_list:
            org = json.loads(organization, encoding="utf-8")
            name = "{city}, {street}, {house}".format(city=org["City"],
                                                      street=org["Street"],
                                                      house=org["House"])

            coords_request = json.dumps({"City": org["City"], "Street": org["Street"], "House": org["House"]})
            coords_resp = sendMessage(host=strings.HOST_GEOCODER,
                                                port=int(strings.PORT_GEOCODER),
                                                message=coords_request)
            coords_resp = json.loads(coords_resp, encoding="utf-8")
            coords = {}
            if (coords_resp["success"] == True):
                coords = coords_resp["result"]

            # check area into database
            check_area = Stores.getStore(hash=org["Hash"])
            if check_area == None:
                print(Stores.createStore({"store_hash": org["Hash"],
                                            "store_name": name.lower(),
                                            "lat": coords["lat"],
                                            "lon": coords["lon"]}))

        return {"success": True}
    except Exception as ex:
        return {"success": False, "error": str(ex)}


if __name__ == "__main__":
    update()
    print(Stores.getAllStores())