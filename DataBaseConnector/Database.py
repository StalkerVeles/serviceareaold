# -*- coding:utf-8 -*-

from sqlalchemy import create_engine, Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import TEXT
from sqlalchemy.orm import sessionmaker
import os.path
from config.strings import database_name

engine = create_engine("sqlite:///{path}/{dbname}".format(path=os.path.dirname(__file__), dbname=database_name),
                       encoding="utf8",
                       echo=False)
Session = sessionmaker(engine, autoflush=True)
Base = declarative_base()

# модели для работы с БД.

class Stores(Base):
    __tablename__ = "store"
    store_hash = Column("store_hash", TEXT, primary_key=True)
    store_name = Column("store_name", TEXT)
    lat = Column("lat", TEXT)
    lon = Column("lon", TEXT)

    def __init__(self, store_hash="", store_name="", lat="", lon=""):
        self.store_hash = store_hash
        self.store_name = store_name
        self.lat = lat
        self.lon = lon

    @classmethod
    def createStore(cls, datas):
        session = Session()
        try:
            obj = cls(store_hash = datas["store_hash"], store_name = datas["store_name"],
                                lat = datas["lat"], lon = datas["lon"])
            session.add(obj)
            session.commit()
            session.close()
            return {"success": True, "error": ""}
        except Exception as ex:
            session.rollback()
            session.close()
            return {"success": False, "error": str(ex)}

    @classmethod
    def getStore(cls, name="", hash=""):
        session = Session()
        query = None
        try:
            if (name == "" and hash != "") or (hash != "" and name != ""):
                query = session.query(cls).filter_by(store_hash=hash).first()
                session.close()
            elif (hash == "" and name != ""):
                query = session.query(cls).filter_by(store_name=name).first()
                session.close()
            else:
                return None
        except:
            pass

        session.close()
        if query != None:
            return {"store_hash": query.store_hash, "store_name": query.store_name, "lat": query.lat, "lon": query.lon}
        else:
            return None

    @classmethod
    def getAllStores(cls):
        session = Session()
        query = session.query(cls).all()
        stores = {}
        if query != None:
            for s in query:
                stores[s.store_name] = {"hash": s.store_hash, "lat": s.lat, "lon": s.lon}
        return stores

    def __repr__(self):
        return "<Organization(name={name}; hash={hash})>".format(name=self.store_name,
                                                                 hash=self.store_hash)

class Areas(Base):
    __tablename__ = "area"
    area_name = Column("area_name", TEXT, primary_key=True)
    area_params = Column("area_params", TEXT)

    def __init__(self, area_name="", params=""):
        self.area_name = area_name
        self.area_params = params

    @classmethod
    def createArea(cls, name="", params=""):
        session = Session()
        try:
            obj = cls(area_name=name, params=params)
            session.add(obj)
            session.commit()
            session.close()
            return {"success": True, "error": ""}
        except Exception as ex:
            session.rollback()
            session.close()
            return {"success": False, "error": str(ex)}

    @classmethod
    def updateArea(cls, name="", params=""):
        session = Session()
        try:
            session.query(cls).filter_by(area_name=name).update({"area_params": params})
            session.commit()
            session.close()
            return {"success": True, "error": ""}
        except Exception as ex:
            session.rollback()
            session.close()
            return {"success": False, "error": str(ex)}

    @classmethod
    def deleteAreas(cls):
        session = Session()
        session.query(cls).delete()
        session.commit()
        session.close()

    @classmethod
    def getArea(cls, name=""):
        session = Session()
        query = None
        try:
            query = session.query(cls).filter_by(area_name=name).first()
        except:
            session.close()
            return None

        session.close()
        if query != None:
            return {"name": query.area_name, "params": query.area_params}
        else:
            return None

    @classmethod
    def getAllAreas(cls):
        session = Session()
        query = session.query(cls).all()
        areas = {}
        if query != None:
            for a in query:
                areas[a.area_name] = a.area_params
        return areas

if __name__ == "__main__":
    Base.metadata.create_all(engine)
    #print(Stores.getAllStores())