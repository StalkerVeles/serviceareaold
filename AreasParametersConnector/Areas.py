# -*- coding:utf-8 -*-

from config.strings import yandex_Oauth_token
from grab import Grab
import re, json
from DataBaseConnector.Database import Areas, Stores

def getCoordsFromYadisk():
    '''
    В данной функции производится скачивание файла с яндекс диска,
    его перекодирование в строку, парсинг с последующим извлечением
    координат и названий зон. Затем формируется список словарей
    по типу {"имя зоны": {"xpoints": [список всех икс координат точек], "ypoints": [список всех игрек координат точек]}}
    :return: areas:
    '''
    g = Grab()
    g.setup(headers={"Authorization": "OAuth %s" % yandex_Oauth_token, 'Depth':'1'})
    try:
        while 1:
            try:
                g.go(url='https://cloud-api.yandex.net/v1/disk/resources/?path=/Конструктор карт Яндекса/Экспорт/')
                break
            except Exception as e:
                pass

        body = g.doc.body.decode()
        files_count = re.findall(r'\d{2}-\d{2}-\d{4}_\d{2}-\d{2}-\d{2}.geojson', body)

        check_dates = []
        for file_name in files_count:
            if files_count.count(file_name) > 1:
                files_count.remove(file_name)
            check_dates.append(file_name.split('.')[0])
        date = max(check_dates)

        fileList = re.split(r'[",:/;]', body)
        for f in fileList:
            if date in f:
                file = f
                break
        path = 'https://cloud-api.yandex.net/v1/disk/resources/download?path=/Конструктор карт Яндекса/Экспорт/%s' % file
        #print("FILE: ", path)

        while 1:
            try:
                g.go(url=path)
                break
            except Exception as e:
                pass

        link = json.loads(g.doc.body.decode())
        link = link["href"]
        while 1:
            try:
                g.go(link)
                break
            except:
                pass
        else:
            pass

        jsonBody = json.loads(g.doc.body.decode())
    except:
        return None

    return jsonBody


def update():
    downloadedFile = getCoordsFromYadisk()
    if downloadedFile != None:
        availablesArea = Areas.getAllAreas().keys()
        storesNames = Stores.getAllStores().keys()
        if (downloadedFile["type"] == "FeatureCollection"):
            feautures = downloadedFile["features"]
            #Areas.deleteAreas()
            for feauture in feautures:
                if feauture["geometry"]["type"] == "Polygon":
                    coordinates = feauture["geometry"]["coordinates"]
                    xpoints = []
                    ypoints = []
                    for coord in coordinates[0]:
                        xpoints.append(coord[0])
                        ypoints.append(coord[1])
                    nameArea = feauture["properties"]["description"].lower()
                    #Areas.createArea(name=nameArea, params=json.dumps({"xpoints": xpoints, "ypoints": ypoints}))
                    if nameArea not in availablesArea and nameArea in storesNames:
                       Areas.createArea(name=nameArea, params=json.dumps({"xpoints": xpoints, "ypoints": ypoints}))
                    elif nameArea in availablesArea and nameArea in storesNames:
                       print("1")
                       re = Areas.updateArea(name=nameArea, params=json.dumps({"xpoints": xpoints, "ypoints": ypoints}))
                       print(re)
        return {"success": True}
    elif downloadedFile == None:
        return {"success": False, "error": "can't load areas file"}
    else:
        return {"success": False, "error": "unknown error into Areas.py"}

# проверяем существует ли в базе такая зона обслуживания, если существует, то
# надо сверить содержимое и если оно отличается от полученного, то перезаписать в базе,
# если не отличается, то не трогать, а если такой зоны ещё нет в базе, то создать новую запись.

if __name__ == "__main__":
    print(update())