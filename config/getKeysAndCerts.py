# -*- coding:utf-8 -*-

import os

def getPathToKey(key=""):
    return os.path.abspath(os.path.dirname(__file__)) + "/" + key

def getPathToCert(cert=""):
    return os.path.abspath(os.path.dirname(__file__)) + "/" + cert