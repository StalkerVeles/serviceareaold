# -*- coding:utf-8 -*-

from turnToAnotherService import sendMessage
from config.strings import HOST_GEOCODER, PORT_GEOCODER
import json
from DataBaseConnector.Database import Areas, Stores

def inPolygon(lon=0, lat=0, lon_area_points=[], lat_area_points=[]):
    '''
    В данной функции производится вычисление попадания точки
    в определенную координатную зону.
    :param lon:
    :param lat:
    :param lon_area_points:
    :param lat_area_points:
    :return c:
    '''
    c = 0
    for i in range(len(lon_area_points)):
        if (((lat_area_points[i] <= lat and lat < lat_area_points[i-1]) or (lat_area_points[i-1] <= lat and lat < lat_area_points[i])) \
                    and (lon > (lon_area_points[i-1] - lon_area_points[i]) * (lat - lat_area_points[i]) / (lat_area_points[i-1] - lat_area_points[i]) + lon_area_points[i])):
            c = 1 - c
    return c

def getServiceStore(address={'city': 'Курган', 'street': 'Проспект Машиностроителей', 'house': '1', 'building': 'а'}):
    resultList = []
    try:
        message = {'type': 'getAddressCoordinates', 'values': address}
        coordinates = json.loads( sendMessage(host=HOST_GEOCODER, port=int(PORT_GEOCODER), message=json.dumps(message)) )
        if coordinates["success"] == True:
            res = coordinates["result"]
            areas = Areas.getAllAreas()
            for a in areas:
                params = json.loads(areas[a])
                if inPolygon(lon=float(res["lon"]), lat=float(res["lat"]), lon_area_points=params["xpoints"], lat_area_points=params["ypoints"]):
                    resultList.append(Stores.getStore(name=a)["store_hash"])
    except:
        pass
    return resultList

def getServiceStoreOldApi(address={"City": "Курган", "Street": "Проспект Машиностроителей", "House": "1А"}):
    resultList = []
    try:
        message = {'type': 'getAddressCoordinates', 'values': address}
        coordinates = json.loads( sendMessage(host=HOST_GEOCODER, port=int(PORT_GEOCODER), message=json.dumps(address)) )
        if coordinates["success"] == True:
            res = coordinates["result"]
            areas = Areas.getAllAreas()
            for a in areas:
                params = json.loads(areas[a])
                if inPolygon(lon=float(res["lon"]), lat=float(res["lat"]), lon_area_points=params["xpoints"], lat_area_points=params["ypoints"]):
                    resultList.append(Stores.getStore(name=a)["store_hash"])
    except:
        pass
    return resultList

def getServiceStoreByCoordinates(coordinates={}):
    resultList = []
    try:
        areas = Areas.getAllAreas()
        for a in areas:
            params = json.loads(areas[a])
            if inPolygon(lon=float(coordinates["lon"]), lat=float(coordinates["lat"]), lon_area_points=params["xpoints"],
                         lat_area_points=params["ypoints"]):
                resultList.append(Stores.getStore(name=a)["store_hash"])
    except:
        pass
    return resultList

if __name__ == "__main__":
    print(1, getServiceStore())
    print(2, getServiceStore(address={"city": "Курган", "street": "5 микрорайон", "house": "33"}))