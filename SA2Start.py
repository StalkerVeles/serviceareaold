# -*- coding:utf-8 -*-
import json
import socket
import ssl
import socketserver
import traceback

# нужно написать документацию

from config.getKeysAndCerts import getPathToCert, getPathToKey
from config import strings
from turnToAnotherService import matchLengthMessage, readMessageFromSocket
from OrganizationsConnector import Organizations
from AreasParametersConnector import Areas
from serviceStoreIdentificator import getServiceStore, getServiceStoreOldApi, getServiceStoreByCoordinates

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        print("Incoming", self.request)
        request = readMessageFromSocket(self.request)
        print(request)
        if request == "PING":
            self.request.sendall(matchLengthMessage("PONG"))
        else:
            try:
                # курган, гоголя, 66 - c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44
                g66 = "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44"
                # курган, гоголя, 36 - ad1d57d35017dd0522da830eb32940e26d7e98ca9caff456162a5db4633c45907679812d228f7fc48114d76d27c9902e0f6a3195ab9ef999f545e125d8c68a3c
                g36 = "ad1d57d35017dd0522da830eb32940e26d7e98ca9caff456162a5db4633c45907679812d228f7fc48114d76d27c9902e0f6a3195ab9ef999f545e125d8c68a3c"

                requestJSON = json.loads(request)
                requestKeys = requestJSON.keys()
                if ("type" in requestKeys):
                    if (requestJSON["type"]) == "UpdateOrgs":
                        res = Organizations.update()
                        self.request.sendall( matchLengthMessage(json.dumps(res)) )

                    elif (requestJSON["type"]) == "UpdateAreas":
                        res = Areas.update()
                        self.request.sendall( matchLengthMessage(json.dumps(res)) )

                    elif (requestJSON["type"]) == "IdentifyServiceStore":
                        if "address" in requestJSON.keys():
                            if requestJSON["address"] != {}:
                                keys = requestJSON["address"].keys()
                                if "city" in keys and "street" in keys and "house" in keys:
                                    storesHashesList = getServiceStore(requestJSON["address"])
                                    if g36 in storesHashesList:
                                        if len(storesHashesList) == 1:
                                            self.request.sendall(matchLengthMessage(json.dumps({"success": True, "result": [g66]})))
                                        elif len(storesHashesList) == 2:
                                            if g66 in storesHashesList:
                                                storesHashesList.remove(g36)
                                                self.request.sendall( matchLengthMessage(json.dumps({"success": True, "result": storesHashesList})) )
                                            else:
                                                storesHashesList.remove(g36)
                                                storesHashesList.append(g66)
                                                self.request.sendall( matchLengthMessage(json.dumps({"success": True, "result": storesHashesList})) )
                                        elif len(storesHashesList) == 3:
                                            storesHashesList.remove(g36)
                                            self.request.sendall(matchLengthMessage(json.dumps({"success": True, "result": storesHashesList})))
                                    else:
                                        self.request.sendall(matchLengthMessage(json.dumps({"success": True, "result": storesHashesList})))
                            else:
                                self.request.sendall( matchLengthMessage(json.dumps({"success": False, "error": "address is empty"})) )
                        else:
                            self.request.sendall( matchLengthMessage(json.dumps({"success": False, "error": "missing the \"address\" tag"})) )
                    elif (requestJSON["type"]) == "IdentifyServiceStoreByCoordinates":
                        if "coordinates" in requestJSON.keys():
                            storesHashesList = getServiceStoreByCoordinates(requestJSON["coordinates"])
                            self.request.sendall(
                                matchLengthMessage(json.dumps({"success": True, "result": storesHashesList})))
                        else:
                            self.request.sendall(
                                matchLengthMessage(json.dumps({"success": False, "error": "incorrect request"})))

                else:
                    # for old requests
                    if len(requestKeys) == 3 and ("City" in requestKeys and "Street" in requestKeys and "House" in requestKeys):
                        if requestJSON["City"] != "" and requestJSON["Street"] != "" and requestJSON["House"] != "":
                            storesHashesList = getServiceStoreOldApi(requestJSON)
                            if g66 in storesHashesList and g36 in storesHashesList and len(storesHashesList) == 2:
                                storesHashesList.remove(g36)
                                self.request.sendall(matchLengthMessage( "01:" + json.dumps({"Exist": True, "HashList":storesHashesList})))
                            elif len(storesHashesList) == 1 and g36 in storesHashesList:
                                self.request.sendall(matchLengthMessage("01:" + json.dumps({"Exist": True, "HashList": [g66]})))
                            else:
                                self.request.sendall(matchLengthMessage("01:" + json.dumps({"Exist": True, "HashList": storesHashesList})))

                        else:
                            self.request.sendall( matchLengthMessage("00:" + json.dumps({"Exist": False, "HashList":[], "success": False, "error": "incorrect request"})) )
                    else:
                        self.request.sendall( matchLengthMessage(json.dumps({"success": False, "error": "incorrect request"})) )

            except Exception as ex:
                #print(traceback.format_exc())
                self.request.sendall( bytes(matchLengthMessage('{"success": false, "error": "%s"}' % str(ex)), 'ascii') )

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    def __init__(self, myaddress, handler, bind_and_activate=True):
        socketserver.TCPServer.__init__(self, myaddress, handler)
        self.logRequests = True

        self.socket = ssl.wrap_socket(socket.socket(),
                                      server_side=True,
                                      certfile=getPathToCert(strings.cert),
                                      keyfile=getPathToKey(strings.key),
                                      ca_certs=getPathToCert(strings.cert),
                                      cert_reqs=ssl.CERT_REQUIRED,
                                      ssl_version=ssl.PROTOCOL_TLSv1_2)

        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def shutdown_request(self, request):
        print("Shutdown")
        request.shutdown(socket.SHUT_WR)

if __name__ == "__main__":
    server = ThreadedTCPServer(("0.0.0.0", 7730), ThreadedTCPRequestHandler)
    server.serve_forever()
